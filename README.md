# DOC 
tech stack : laravel 10 + vue js 3 inertia + bootsrap 5
1. Component bisa didapatkan di [prime vue](https://primevue.org)
2. dokumentasi inertia ada di [inertia vue 3](https://inertiajs.com)
3. dokumentasi bootstrap ada di [bootstrap 5](https://getbootstrap.com/docs/5.3/getting-started/introduction/)
4. icon bisa didapatkan di [font awesome](https://fontawesome.com/icons/)
5. documentasi notif swal di [swal](https://sweetalert2.github.io/)

# ADMIN
## hierarchy  
- mengatur semua UI yang ada dalam ASLAB
- CRUD semua menu 

UI ADMIN Mengatur semua ui yang ada di ASLAB

   
# ASLAB
## hierarchy 
- bisa mengatur aktif atau tidak nya sesi praktikum
- memberi hak praktikan untuk online
- memberi nilai kepada praktikan 
- melihat jalanya praktikum
- ...

# UI ASLAB
Component bisa didapatkan di [prime vue](https://primevue.org)
### 1. Menu Home 
- card ( total praktikan ) -> didapat dari total praktikan yang diambil
- card ( pertemuan ) -> didapat dari pertemuan ke berapa saat praktikum dimulai 
- card ( praktikum ) -> didapat dari nama praktikum yang aktif
- card ( total aslab ) -> didapat dari total aslab yang aktif

### 2. Menu Praktikum
- breadcrumbs 
    - praktikum
      - header text ( praktikum {nama_praktikum_aktif}) -> jika sekarang praktikum nya basis data maka nama_praktikum_aktif yaitu basis data
      - data table dengan colom :
        - pertemuan (berisi total pertemuan dalam praktikum)
        - sesi ( total sesi dalam pertemuan )
        - jawaban ( total jawaban dalam setiap pertemuan )
    - pertemuan 
    - pengisian soal 
### 3. Menu List Praktikan
- table list praktikan
  colom tabel :
    - no 
    - npm 
    - nama
    - kelas 
    - sesi (berisi no sesi ,hari dan jam )
    - aksi ( masuk ke preview sebuah card dimana ada deskripsi dari praktikan)
      berisi :
      - nama 
      - npm 
      - no telp 
      - dan gambar profile praktikan
  
### 4. Menu Pelanggaran
- select pertemuan praktikum ( misal pertemuan 1 - 7  jika di select pertemuan 1 maka akan muncul tabel penilaian untuk praktikum pertemuan 1)
- colom tabel :
    - no 
    - npm 
    - nama
    - praktikum ( Button yang berisi pelanggaran praktikan default nya belum terisi jika ditekan muncul modal untuk mengisi kan pelanggaran nya note: dalam bentuk persen)
    - asistensi ( Button yang berisi pelanggaran praktikan default nya belum terisi jika ditekan muncul modal untuk mengisi kan pelanggaran nya note: dalam bentuk persen)
### 5. Menu Nilai
- select pertemuan praktikum ( misal pertemuan 1 - 7  jika di select pertemuan 1 maka akan muncul tabel penilaian untuk praktikum pertemuan 1)
- colom tabel :
    - no 
    - npm 
    - nama
    - praktikum ( Button yang berisi nilai praktikan default nya "belum terisi" jika ditekan muncul modal untuk mengisi kan nilai nya)
    - asistensi ( Button yang berisi nilai praktikan default nya "belum terisi" jika ditekan muncul modal untuk mengisi kan nilai nya)
### 6. Menu Profile



# PRAKTIKAN
## hierarchy 
- melakukan praktikum
- mengumpulkan nilai praktikum
- mengatur profile 
- melihat nilai praktikum


## FLOW PRAKTIKUM USER
user menunggu di route '/praktikum' di side bar praktikum 
dan ketika aktif maka akan muncul card praktikum user akan menekan masuk setelah masuk user diberikan soal dan template untuk menjawab dan form untuk menaruh file RAR yaitu yang berisi tugas praktikum dll sampai waktu dari praktikum habis jika telat 10 menit maka akan dikurangi 10% nilai 
jika telat lebih dari 10 menit maka nilai akan 60

  ## Sistem Agile
  
  ### hafidz dev 
    12 februari 
    - menambahkan seeder minimal 15 
    13 februari - 14 februari
    - Menu Home
  
  ### achmad dev
    12 februari 
    - menyambungkan database
    13 februari 
    - Menu Nilai