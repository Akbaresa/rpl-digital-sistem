<?php

namespace App\Http\Controllers;

use App\Models\Pertemuan;
use App\Models\Praktikum;
use Hamcrest\Core\HasToString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class AslabController extends Controller
{

    public function index(){
        return Inertia::render('aslab/Beranda');
    }

    public function nilai(){
        return Inertia::render('aslab/Nilai');
    }

    public function praktikum() {
        $praktikum_aktif = Praktikum::select('nama_praktikum')
        ->where('status' , 'aktif')->first();
        return Inertia::render('aslab/praktikum/PraktikumSubPage', [
            'praktikum_aktif' => $praktikum_aktif
        ]);
    }

    public function form_praktikum(){
        return Inertia::render('aslab/praktikum/FormPraktikum' ,[

        ]);
    }

    public function pertemuan_praktikum(){

        $praktikum_aktif = Praktikum::select('nama_praktikum')
        ->where('status' , 'aktif')->first();

        $pertemuan = Pertemuan::select('pertemuan.*')
        ->join('praktikum', 'praktikum.id', 'pertemuan.praktikum_id')
        ->where('praktikum.status', 'aktif')
        ->get()
        ->map(function ($pertemuan) {
            return [
                'no_pertemuan' => $pertemuan->no_pertemuan,
                'status' => $pertemuan->status
            ];
        });

        return Inertia::render('aslab/praktikum/PertemuanPraktikum', [
            'pertemuan' => $pertemuan,
            'praktikum' => $praktikum_aktif
        ]);
    }

    public function sesi_pertemuan($no_pertemuan){
        $praktikum_aktif = Praktikum::select('nama_praktikum')
        ->where('status' , 'aktif')->first();

        return Inertia::render('aslab/praktikum/SesiPertemuan',[
            'no_pertemuan' => $no_pertemuan,
            'praktikum' => $praktikum_aktif
        ]);
    }

    public function pelanggaran(){
        return Inertia::render('aslab/Pelanggaran');
    }

    public function praktikan(){
        return Inertia::render('aslab/ListPraktikan');
    }
    public function profil(){
        return Inertia::render('aslab/Profile');
    }

    public function set_status_pertemuan(Request $request){
        $request->validate([
            'status' => 'required',
            'no_pertemuan' => 'required'
        ]);

        Pertemuan::join('praktikum', 'praktikum.id', 'pertemuan.praktikum_id')
        ->where('praktikum.status', 'aktif')
        ->update(['pertemuan.status' => $request->status]);

        return to_route('aslab.praktikum.pertemuan');
    }


}
