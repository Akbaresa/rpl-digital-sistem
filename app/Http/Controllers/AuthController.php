<?php

namespace App\Http\Controllers;

use App\Models\Aslab;
use App\Models\Praktikan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;
class AuthController extends Controller
{
    public function login_proses(Request $req)
    {
        try {
            $req->validate([
                'npm' => 'required',
                'password' => 'required'
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->with(['jenis' => 'error', 'pesan' => 'Username atau Password salah'])->withInput();
        }
        $cekPraktikan = Praktikan::where('npm', $req->npm)->first();
        if ($cekPraktikan && Auth::guard('praktikan')->attempt(['npm' => $req->npm, 'password' => $req->password])) {
            return to_route('praktikan.dashboard')->with('pesan' , 'Berhasil Login, Selamat Datang Kembali.');
        }
        $cekAslab = Aslab::where('npm', $req->npm)->first();
        if ($cekAslab && Auth::guard('aslab')->attempt(['npm' => $req->npm, 'password' => $req->password])) {
            return to_route('aslab.dashboard')->with('pesan' , 'Berhasil Login, Selamat Datang Kembali');
        }
    
        return redirect()->back()->withInput()->with('pesan', 'Username atau Password salah');
    }
}
