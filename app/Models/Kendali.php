<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kendali extends Model
{
    use HasFactory;
    protected $table = 'kendali';
    protected $hidden = ['id'];
    
    public function praktikan()
    {
        return $this->belongsTo(Praktikan::class);
    }

    public function aslab()
    {
        return $this->belongsTo(Aslab::class);
    }
}
