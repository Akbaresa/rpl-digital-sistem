<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelanggaran extends Model
{
    use HasFactory;
    protected $table = 'pelanggaran';
    protected $hidden = ['id'];
    
    public function sesi()
    {
        return $this->hasMany(Sesi::class);
    }

    public function praktikan()
    {
        return $this->hasMany(Praktikan::class);
    }
}
