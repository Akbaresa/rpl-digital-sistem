<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenilaianPraktikum extends Model
{
    use HasFactory;
    protected $table = 'penilaian_praktikum';
    protected $hidden = ['id'];

    public function alasanOnline()
    {
        return $this->belongsTo(AlasanOnline::class);
    }

    public function jawaban()
    {
        return $this->belongsTo(Jawaban::class);
    }

    public function sesi()
    {
        return $this->hasMany(Sesi::class);
    }

    public function praktikan()
    {
        return $this->hasMany(Praktikan::class);
    }
}
