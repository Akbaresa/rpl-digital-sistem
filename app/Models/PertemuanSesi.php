<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PertemuanSesi extends Model
{
    use HasFactory;
    protected $table = 'pertemuan_sesi';
    protected $hidden = ['id'];
    
    public function pertemuan()
    {
        return $this->belongsTo(Pertemuan::class);
    }

    public function sesi()
    {
        return $this->belongsTo(Sesi::class);
    }
}
