<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class Praktikan extends Model implements AuthenticatableContract
{
    use HasFactory;
    use Authenticatable;
    protected $table = 'praktikan';
    protected $hidden = ['id'];
    
    public function kendali()
    {
        return $this->hasMany(Kendali::class);
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function pelanggaran()
    {
        return $this->belongsTo(Pelanggaran::class);
    }

    public function penilaianPraktikum()
    {
        return $this->belongsTo(PenilaianPraktikum::class);
    }
}
