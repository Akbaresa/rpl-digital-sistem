<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sesi extends Model
{
    use HasFactory;
    protected $table = 'sesi';
    protected $hidden = ['id'];
    
    public function pertemuanSesi()
    {
        return $this->hasMany(PertemuanSesi::class);
    }

    public function soal()
    {
        return $this->belongsTo(Soal::class);
    }

    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    public function penilaianPraktikum()
    {
        return $this->belongsTo(PenilaianPraktikum::class);
    }

    public function pelanggaran()
    {
        return $this->belongsTo(Pelanggaran::class);
    }
}
