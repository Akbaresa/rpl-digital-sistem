<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('penilaian_praktikum', function (Blueprint $table) {
            $table->id();
            $table->string('nilai');
            $table->enum('status_kehadiran', ['hadir', 'tidak', 'online'])->default('tidak');
            $table->foreignId('praktikan_id');
            $table->foreignId('sesi_log_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('penilaian_praktikum');
    }
};
