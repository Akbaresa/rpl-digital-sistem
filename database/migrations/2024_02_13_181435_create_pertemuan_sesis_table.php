<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pertemuan_sesi', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pertemuan_id');
            $table->foreignId('sesi_id');
            $table->enum('status' ,['aktif' , 'tidak'])->default('tidak');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pertemuan_sesis');
    }
};
