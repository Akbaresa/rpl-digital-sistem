<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Admin::insert([
            [
                'nama' => 'esa',
                'password' => bcrypt('123')
            ],
            [
                'nama' => 'arum',
                'password' => bcrypt('123')
            ],
            [
                'nama' => 'havidz',
                'password' => bcrypt('123')
            ],
            [
                'nama' => 'cemet',
                'password' => bcrypt('123')
            ],
            [
                'nama' => 'zainul',
                'password' => bcrypt('123')
            ],
            [
                'nama' => 'safir',
                'password' => bcrypt('123')
            ],
            [
                'nama' => 'feri',
                'password' => bcrypt('123')
            ]
        ]);
    }
}
