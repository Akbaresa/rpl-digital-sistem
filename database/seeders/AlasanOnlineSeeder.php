<?php

namespace Database\Seeders;

use App\Models\AlasanOnline;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AlasanOnlineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        AlasanOnline::insert([
            [
                'nama_alasan' => 'sakit',
                'path' => ''
            ],
            [
                'nama_alasan' => 'tidak ada motor',
                'path' => ''
            ],
            [
                'nama_alasan' => 'bekerja',
                'path' => ''
            ],
            [
                'nama_alasan' => 'menjaga toko',
                'path' => ''
            ]
        ]);
    }
}
