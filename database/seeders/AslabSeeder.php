<?php

namespace Database\Seeders;

use App\Models\Aslab;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AslabSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Aslab::insert([
            [
                'nama' => 'Maulana Akbar Esa Putra',
                'npm' => '12',
                'email' => 'takuto@gmail.com',
                'password' => bcrypt('12'),
                'status' => 'aktif'
            ],
            [
                'nama' => 'Hafidz Ubaidillah',
                'npm' => '02',
                'email' => 'hafiz@gmail.com',
                'password' => bcrypt('12'),
                'status' => 'aktif'
            ],
            [
                'nama' => 'Achmad',
                'npm' => '03',
                'email' => 'achmad@gmail.com',
                'password' => bcrypt('12'),
                'status' => 'aktif'
            ],
            [
                'nama' => 'ferry irawan',
                'npm' => '04',
                'email' => 'ferry@gmail.com',
                'password' => bcrypt('12'),
                'status' => 'aktif'
            ],
            [
                'nama' => 'safir',
                'npm' => '05',
                'email' => 'safir@gmail.com',
                'password' => bcrypt('12'),
                'status' => 'aktif'
            ],
            [
                'nama' => 'arum',
                'npm' => '06',
                'email' => 'arum@gmail.com',
                'password' => bcrypt('12'),
                'status' => 'aktif'
            ],
            [
                'nama' => 'zainul',
                'npm' => '07',
                'email' => 'takuto12@gmail.com',
                'password' => bcrypt('12'),
                'status' => 'aktif'
            ],
        ]);
    }
}
