<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\PertemuanModul;
use App\Models\PertemuanSesi;
use App\Models\Praktikum;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            AdminSeeder::class,
            PraktikanSeeder::class,
            AslabSeeder::class,
            PertemuanSeeder::class,
            PraktikumSeeder::class,
            SesiSeeder::class,
            PertemuanModulSeeder::class,
            PertemuanSesiSeeder::class
        ]);
    }
}
