<?php

namespace Database\Seeders;

use App\Models\Kendali;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KendaliSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $praktikanId = 5000;
        $aslabId = 9000;

        // Buat 20 baris data
        for ($i = 0; $i < 20; $i++) {
            Kendali::insert([
                'praktikan_id' => $praktikanId + $i,
                'aslab_id' => $aslabId + ($i % 8) // Untuk mengulangi aslab_id dari 9000 hingga 9007
            ]);
        }
    }
}
