<?php

namespace Database\Seeders;

use App\Models\Modul;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ModulSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 14; $i++) {
            Modul::insert([
                'no_modul' => $i,
                'slug' => 'modul-' . $i,
                'deskripsi' => 'Deskripsi modul ' . $i
            ]);
        }
    }
}
