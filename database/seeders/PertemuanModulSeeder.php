<?php

namespace Database\Seeders;

use App\Models\PertemuanModul;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PertemuanModulSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        for ($i = 1; $i <= 14; $i++) {
            $modul1 = $i * 2 - 1; // Pertemuan ganjil
            $modul2 = $i * 2; // Pertemuan genap

            PertemuanModul::insert([
                ['pertemuan_id' => $i, 'modul_id' => $modul1],
                ['pertemuan_id' => $i, 'modul_id' => $modul2],
            ]);
        }
    }
}
