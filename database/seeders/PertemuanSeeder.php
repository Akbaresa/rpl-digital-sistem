<?php

namespace Database\Seeders;

use App\Models\Pertemuan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PertemuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Pertemuan::insert([
            [
                'no_pertemuan' => '1',
                'praktikum_id' => '1',
                'status' => 'aktif'
            ],
            [
                'no_pertemuan' => '2',
                'praktikum_id' => '1',
                'status' => 'tidak'
            ],
            [
                'no_pertemuan' => '3',
                'praktikum_id' => '1',
                'status' => 'tidak'
            ],
            [
                'no_pertemuan' => '4',
                'praktikum_id' => '1',
                'status' => 'tidak'
            ],
            [
                'no_pertemuan' => '5',
                'praktikum_id' => '1',
                'status' => 'tidak'
            ],
            [
                'no_pertemuan' => '6',
                'praktikum_id' => '1',
                'status' => 'tidak'
            ],
            [
                'no_pertemuan' => '7',
                'praktikum_id' => '1',
                'status' => 'tidak'
            ],
        ]);
    }
}
