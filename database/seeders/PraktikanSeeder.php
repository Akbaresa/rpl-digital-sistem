<?php

namespace Database\Seeders;

use App\Models\Praktikan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PraktikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Bersihkan tabel sebelum menambahkan data baru
        Praktikan::truncate();

        // Data praktikan
        $data = [
            [
                'nama' => 'esa',
                'npm' => '123',
                'email' => 'takuto@gmail.com',
                'password' => bcrypt('12'),
                'no_wa' => '08580667'
            ],
            [
                'nama' => 'akbar',
                'npm' => '12',
                'email' => 'takuto12@gmail.com',
                'password' => bcrypt('12'),
                'no_wa' => '0858066712'
            ],
            // Tambahkan data praktikan lainnya sesuai kebutuhan
        ];

        // Tambahkan 18 data praktikan tambahan untuk total 20 praktikan
        for ($i = 1; $i <= 18; $i++) {
            $data[] = [
                'nama' => 'Praktikan ' . $i,
                'npm' => '06.0212' . $i, // Contoh npm acak antara 100 dan 999
                'email' => 'praktikan' . $i . '@example.com',
                'password' => bcrypt('password'), // Kata sandi default
                'no_wa' => '08120667' . $i // Contoh nomor WhatsApp acak
            ];
        }

        // Masukkan semua data ke dalam tabel praktikan
        Praktikan::insert($data);
    }
}
