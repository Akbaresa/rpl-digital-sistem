<?php

namespace Database\Seeders;

use App\Models\Praktikum;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PraktikumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Praktikum::insert([
            [
                'tahun' => '2024-03-04',
                'nama_praktikum' => 'Basis Data',
                'status' => 'aktif' 
            ],
            [
                'tahun' => '2024-03-04',
                'nama_praktikum' => 'Pemrograman Berorientasi Objek',
                'status' => 'tidak' 
            ]
        ]);
    }
}
