<?php

namespace Database\Seeders;

use App\Models\Sesi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SesiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Sesi::insert([
            [
                'no_sesi' => '1',
                'waktu_mulai' => Carbon::parse('next Wednesday')->setHour(16)->setMinute(10),
                'waktu_selesai' => Carbon::parse('next Wednesday')->setHour(17)->setMinute(0),
                'pertemuan_id' => '1'
            ],
            [
                'no_sesi' => '2',
                'waktu_mulai' => Carbon::parse('next Friday')->setHour(15)->setMinute(10),
                'waktu_selesai' => Carbon::parse('next Friday')->setHour(16)->setMinute(0),
                'pertemuan_id' => '1'
            ],
            [
                'no_sesi' => '3',
                'waktu_mulai' => Carbon::parse('next Friday')->setHour(18)->setMinute(10),
                'waktu_selesai' => Carbon::parse('next Friday')->setHour(19)->setMinute(0),
                'pertemuan_id' => '2'
            ]
        ]);
    }
}
