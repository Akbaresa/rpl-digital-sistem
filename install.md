
# How to Run Program

- copy file .env.example dan paste

- rename file yang dipaste menjadi .env

- composer install
  
- npm install / npm install --force
  
- php artisan key:generate
  
- npm run dev
  
- nyalakan database

- php artisan migrate / php artisan migrate:fresh --seed

- php artisan db:seed

- php artisan serve
  
- masuk kedala Server running dari php artisan serve


# cara mengerjakan
- setelah sudah run program 
  
- buat branch baru dengan
  bash : git checkout -b {nama branch} 

- kerjakan sesuai fitur 
- setelah selesai mengerjakan fitur 
- lanjut dengan 
  - bash : git add .
  - bash : git commit -m "{nama fitur}"
  - bash : git push {nama remote} {nama branch} 

  note : 
  - file yang di push harus sesuai fitur
  - tidak boleh menyentuh file fitur yang lain
  - jika ada bug laporkan di grub

  ## standart penamaan commit :
  - "add {nama fitur}" jika menambahkan fitur
  - "fix {nama fitur}" jika memperbaiki fitur jika ada bug
  - "update {nama fitur}" jika melakukan pembaruan pada fitur yang sudah ada
  - "remove {nama fitur}" jika menghapus fitur
  - "refactor {nama fitur}" jika melakukan perbaikan pada struktur kode tanpa mengubah fungsionalitasnya
  - "docs: {deskripsi}" jika menambahkan atau memperbarui dokumentasi
  - "test: {deskripsi}" jika menambahkan atau memperbarui pengujian


  

