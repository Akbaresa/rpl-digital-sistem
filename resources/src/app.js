import './bootstrap';
import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/vue3'
import 'primevue/resources/themes/aura-light-green/theme.css'
import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice'
import SideBar from './Pages/aslab/shared/SideBar.vue'

createInertiaApp({
  resolve: name => {
      const pages = import.meta.glob('./Pages/**/*.vue', { eager: true })
      let page = pages[`./Pages/${name}.vue`].default
  
      if( page.layout === undefined ){
        page.layout = SideBar;
      }
  
      return pages[`./Pages/${name}.vue`]
  },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin)
      .use(PrimeVue)
      .use(ToastService)
      .mount(el)
  },
  progress: {
    color: 'blue',
    includeCSS: true,
    showSpinner: true,
  },
})