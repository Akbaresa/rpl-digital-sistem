// ProductService.js

// Simulasi data produk
const productsData = [
    { Nama: '001', Npm: 'Product 1', Praktikum: 80, Asistensi: 10 },
    { Nama: '002', Npm: 'Product 2', Praktikum: 90, Asistensi: 20 },
    { Nama: '003', Npm: 'Product 3', Praktikum: 79, Asistensi: 15 },
    { Nama: '004', Npm: 'Product 4', Praktikum: 78, Asistensi: 8 },
    { Nama: '005', Npm: 'Product 5', Praktikum: 78, Asistensi: 12 }
];

export const ProductService = {
    // Fungsi untuk mendapatkan data produk (versi mini)
    getProductsMini() {
        // Menggunakan Promise untuk simulasi asynchronicity (misalnya, mengambil data dari API)
        return new Promise((resolve, reject) => {
            // Simulasi delay
            setTimeout(() => {
                resolve(productsData); // Mengembalikan data produk
            }, 1000); // Simulasi delay selama 1 detik (1000 milidetik)
        });
    }
};
