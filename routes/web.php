<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AslabController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Middleware\EnsureTokenIsValid;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::inertia('/login', 'LoginPage');
Route::post('/login-proses', [AuthController::class , 'login_proses'])->name('login');

// Route Aslab
Route::middleware('aslab')->group(function () {
    Route::prefix('/')->name('aslab.')->group(function () {
        Route::get('/', [AslabController::class, 'index'])->name('dashboard');

        Route::prefix('/praktikum')->name('praktikum.')->group(function (){
            Route::get('/', [AslabController::class, 'praktikum'])->name('dashboard');
            Route::get('/pertemuan', [AslabController::class, 'pertemuan_praktikum'])->name('pertemuan');
            Route::get('/pertemuan{no_pertemuan}', [AslabController::class , 'sesi_pertemuan'])->name('sesi');
            Route::get('/form', [AslabController::class , 'form_praktikum'])->name('form');
            
            Route::post('/set-status-pertemuan', [AslabController::class, 'set_status_pertemuan']);
        });

        Route::get('/list-praktikan', [AslabController::class, 'praktikan'])->name('list-praktikan');

        Route::prefix('/nilai')->name('nilai.')->group(function (){
            Route::get('/', [AslabController::class, 'nilai'])->name('index');
            Route::get('/pertemuan1', [AslabController::class, 'nilai1'])->name('nilai.pertemuan1');
        });
        
        Route::get('/pelanggaran', [AslabController::class, 'pelanggaran'])->name('pelanggaran');
        Route::get('/profile', [AslabController::class, 'profil'])->name('profil');
    });
});

